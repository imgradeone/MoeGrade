# MoeGrade

A flat + modern Speed Dial thumb pack designed for Vivaldi Browser, by imgradeone and MoeAria

![demo](demo.png "demo")

---

All brands belong to their owners, and images from MoeGrade are only designed for customizing browser preview images (or anything for customizing). Please don't use it for anything else. Just do customize thing with them.
